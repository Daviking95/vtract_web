// (function ($) {

  var data;
  var firebaseDb = firebase.firestore();
  let filter_state = "edo";
  let filter_df = "2020-01-01";
  let dataResult = {};

  var colRef = firebaseDb.collection("vtrack_media_center").doc("election_result_data").collection("results");

  var docRef = firebaseDb.collection("vtrack_media_center").doc("election_result_data");

  getElectionResult(true);

  $(".election_filter").click(function () {
    filter_state = $(".filter_state_election").val();
    filter_df = $(".filter_df_election").val();

    console.log(`"I am listening"${filter_state} ${filter_df.substring(0,4)}`);

    getElectionResult(true);

  });

  function getElectionResult(isSearch) {

    if (isSearch) {
      dataResult = {};
      docRef.get().then(function (doc) {
        if (doc.exists) {
          data = doc.data();
          console.log(data['results']);
          console.log("Document data:", doc.data());

          var elementArray = data['results'];

          elementArray.forEach(element => {
            if (element.state === filter_state && element.year === filter_df.substring(0,4)) {
              
              window.setInterval(function(){
                showChart(element);
              // setInterval(showChart(element), 500);
              // showChart(element);

              showChart2(element);

            }, 5000);


              console.log(element.voters_turnout);

              $("#uncollected_pvc").numScroll({
                number: numberWithCommas(element.uncollected_pvc)
              });

              $("#pop_data").numScroll({
                number: numberWithCommas(element.registered_voters)
              });

              $("#reg_voters").numScroll({
                number: numberWithCommas(element.registered_voters)
              });

              $("#accr_voter").numScroll({
                number: numberWithCommas(element.accredited_voters)
              });

              $("#pol_parties").numScroll({
                number: numberWithCommas(element.number_of_parties)
              });

              $("#pvc_col").numScroll({
                number: numberWithCommas(element.pvc_collected)
              });

              $("#voters_per_pol_parties").numScroll({
                number: numberWithCommas(element.voters_per_pol_parties)
              });

              $("#polling_units").numScroll({
                number: numberWithCommas(element.polling_units)
              });

              $("#voters_turnouts").numScroll({
                number: numberWithCommas(element.voters_turnout)
              });

              console.log(element.title);
              $('#title').text(element.title);

              return;

            }else {
              // alert("No data for your filter");
            }
          });

        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    } else {
      docRef.get().then(function (doc) {
        if (doc.exists) {
          data = doc.data();
          console.log(data['results']);
          console.log("Document data:", doc.data());

          var element = data['results'][0];

          window.setInterval(function(){
            showChart(element);

          // setInterval(showChart(element), 500);

          // showChart(element);

          showChart2(element);

        }, 5000);


          console.log(element.voters_turnout);

          $("#uncollected_pvc").numScroll({
            number: numberWithCommas(element.uncollected_pvc)
          });

          $("#pop_data").numScroll({
            number: numberWithCommas(element.registered_voters)
          });

          $("#reg_voters").numScroll({
            number: numberWithCommas(element.registered_voters)
          });

          $("#accr_voter").numScroll({
            number: numberWithCommas(element.accredited_voters)
          });

          $("#pol_parties").numScroll({
            number: numberWithCommas(element.number_of_parties)
          });

          $("#pvc_col").numScroll({
            number: numberWithCommas(element.pvc_collected)
          });

          $("#voters_per_pol_parties").numScroll({
            number: numberWithCommas(element.voters_per_pol_parties)
          });

          $("#polling_units").numScroll({
            number: numberWithCommas(element.polling_units)
          });

          $("#voters_turnouts").numScroll({
            number: numberWithCommas(element.voters_turnout)
          });

          console.log(element.title);
          $('#title').text(element.title);

          // });
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });
    }

  }

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  const formatCash = n => {
    if (n < 1e3) return n;
    if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + "K";
    if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + "M";
    if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + "B";
    if (n >= 1e12) return +(n / 1e12).toFixed(1) + "T";
  };

  function showingChart(element) {
    var Data = [{
        label: `UNCOLLECTED PVC : ${element.uncollected_pvc}`,
        value: element.uncollected_pvc
      },
      {
        label: `REGISTERED VOTERS : ${element.registered_voters}`,
        value: element.registered_voters
      }
    ];
    var browsersChart = Morris.Donut({
      element: 'chartContainer',
      data: Data,
      formatter: function (value, data) {
        return value;
      }
    });

    // browsersChart.options.data.forEach(function (label, i) {
    //   var legendItem = $('<span></span>').text(label['label'] + " ( " + label['value'] + " )").prepend('<br><span>&nbsp;</span>');
    //   legendItem.find('span')
    //     .css('backgroundColor', browsersChart.options.colors[i])
    //     .css('width', '20px')
    //     .css('display', 'inline-block')
    //     .css('margin', '5px');
    //   $('#legend').append(legendItem)
    // });
  }

  function showChart(element) {

    var chart = new Chartist.Pie('#chartContainer', {
      series: [element.uncollected_pvc, element.registered_voters, ],
      labels: [`UNCOLLECTED PVC : ${element.uncollected_pvc}`, `REGISTERED VOTERS : ${element.registered_voters}`]
    }, {
      donut: true,
      showLabel: true,
    });

    chart.on('draw', function(data) {
      if(data.type === 'slice') {
        var pathLength = data.element._node.getTotalLength();
        data.element.attr({
          'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });
        var animationDefinition = {
          'stroke-dashoffset': {
            id: 'anim' + data.index,
            dur: 1000,
            from: -pathLength + 'px',
            to:  '0px',
            easing: Chartist.Svg.Easing.easeOutQuint,
            fill: 'freeze'
          }
        };
    
        if(data.index !== 0) {
          animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
        }
        data.element.attr({
          'stroke-dashoffset': -pathLength + 'px'
        });
        data.element.animate(animationDefinition, false);
      }
    });
    chart.on('created', function() {
      if(window.__anim21278907124) {
        clearTimeout(window.__anim21278907124);
        window.__anim21278907124 = null;
      }
      window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
    });
  }

  function showChart2(element) {

    var chart = new Chartist.Pie('#chartContainer2', {
      series: [element.uncollected_pvc, element.registered_voters, ],
      labels: [`UNCOLLECTED PVC : ${element.uncollected_pvc}`, `REGISTERED VOTERS : ${element.registered_voters}`]
    }, {
      donut: true,
      showLabel: true,
    });
  
    chart.on('draw', function (data) {
      if (data.type === 'slice') {
        // Get the total path length in order to use for dash array animation
        var pathLength = data.element._node.getTotalLength();
  
        // Set a dasharray that matches the path length as prerequisite to animate dashoffset
        data.element.attr({
          'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
        });
  
        // Create animation definition while also assigning an ID to the animation for later sync usage
        var animationDefinition = {
          'stroke-dashoffset': {
            id: 'anim' + data.index,
            dur: 1000,
            from: -pathLength + 'px',
            to: '0px',
            easing: Chartist.Svg.Easing.easeOutQuint,
            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
            fill: 'freeze'
          }
        };
  
        // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
        if (data.index !== 0) {
          animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
        }
  
        // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us
        data.element.attr({
          'stroke-dashoffset': -pathLength + 'px'
        });
  
        // We can't use guided mode as the animations need to rely on setting begin manually
        // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
        data.element.animate(animationDefinition, false);
      }
    });
  
    chart.on('created', function () {
      if (window.__anim21278907124) {
        clearTimeout(window.__anim21278907124);
        window.__anim21278907124 = null;
      }
      window.__anim21278907124 = setTimeout(chart.update.bind(chart), 10000);
    });
  }

  getElectionResult(true);

// })(jQuery)