// JQuery
window.$(document).ready(function () {

var firebaseDb = firebase.firestore();
  
  var docRef = firebaseDb.collection("vtrack_media_center");

  gettingAllPhotos("media_photos", true);

  gettingAllPhotos("youtube_links", false);

  gettingIndexSlider("index_sliders");

  async function gettingIndexSlider(docString) {

    var indexSliderData;

    const promise = new Promise((resolve, reject) => {

      docRef.doc(docString).get().then(function (doc) {
        if (doc.exists) {
          indexSliderData = doc.data();

          resolve(indexSliderData)
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });

    });

    let result = await promise;

    displaySliderResults(result);
    return result;
  }

  function displaySliderResults(result) {

    var elementArray = result['data_array'];

    for (let index = 0; index < elementArray.length; index++) {
      const element = elementArray[index];

      if(index == 0){
        $('#indexSliders').append('<div class="slider-holder__overall-pop count-numbers"><p class="paragraph-1">'+ element.hashtag +'</p><h2 class="heading-12 margin-bottom-small">'+ element.numbers+'</h2><p class="paragraph-1">'+ element.title +'</p></div>');
      }else {
        $('#indexSliders').append('<div class="slider-holder__overall count-numbers"><p class="paragraph-1">'+ element.hashtag +'</p><h2 class="heading-12 margin-bottom-small color-secondary">'+ element.numbers+'</h2><p class="paragraph-1">'+ element.title +'</p></div>');
      }
      
    }

    showSlides();

  }

  async function gettingAllPhotos(docString, isPhoto) {

    var mediaAssetList;

    const promise = new Promise((resolve, reject) => {

      docRef.doc(docString).get().then(function (doc) {
        if (doc.exists) {
          mediaAssetList = doc.data();

          resolve(mediaAssetList)
        } else {
          // doc.data() will be undefined in this case
          console.log("No such document!");
        }
      }).catch(function (error) {
        console.log("Error getting document:", error);
      });

    });

    let result = await promise;

    isPhoto ? displayResult(result) : displayVideoResult(result);
    return result;
  }

  function displayResult(result) {

    result["mediaAsset"].forEach(element => {

      var _agentImageRef = firebase.storage().refFromURL(element);
      _agentImageRef.getDownloadURL().then(function (url) {

        $('#get_all_photos').append("<a data-fancybox='gallery' href=" + url + "><img src=" + url + " class='media-picture__img' /></a>");

        $(".media-picture > a").slice(0, 6).show();

      });


    });

  }

  function displayVideoResult(result) {

    result["ylinks"].forEach(element => {

        $('#get_all_videos').append('<a data-fancybox data-width="640" data-height="360" href="#"><iframe class="media-picture__img" width="400" height="401" src="'+ element +'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></a>');

        $(".media-video > a").slice(0, 3).show();

    });
  }

  // Side nav toggler ===================================================================
  var $sideNavToggleBtn = $(".side-navigation-buttons__close");
  $sideNavToggleBtn.click(function () {
    var $sideNavToggleBtnText = $(this).text();
    if ($sideNavToggleBtnText === "Close") {
      $(this).text("Open");
    } else {
      $(this).text("Close");
    }
    $(".side-navigation-buttons").toggleClass("close");
  });

  // Map toggler
  var $electionViolenceData = $("#electionViolenceData");
  var $currentElections = $("#currentElections");
  var $nevrSituationRoom = $("#nevrSituationRoom");

  var $btnMapToggler = $("#map__toggler button");
  var $btnElectionViolenceData = $("#btnElectionViolenceData");
  var $btnCurrentElections = $("#btnCurrentElections");
  var $btnNevrSituationRoom = $("#btnNevrSituationRoom");

  var $activeMapButton = "btn--map-toggler--active";

  //   Map toggler ==> toggling active class on buttons whwn clicked
  $btnMapToggler.click(function () {
    $(this).siblings().removeClass($activeMapButton);
    $(this).addClass($activeMapButton);
  });

  //   Map toggler ==> Changing map based on button clicked
  $btnElectionViolenceData.click(function () {
    $(".map_center").show();

    $electionViolenceData.siblings().fadeOut(200);
    $electionViolenceData.delay(100).fadeIn();
  });

  $btnCurrentElections.click(function () {
    $(".map_center").hide();
    $currentElections.siblings().fadeOut(200);
    $currentElections.delay(100).fadeIn();
  });

  $btnNevrSituationRoom.click(function () {
    $(".map_center").hide();
    $nevrSituationRoom.siblings().fadeOut(200);
    $nevrSituationRoom.delay(100).fadeIn();
  });

  $(".side-navigation-buttons__link--1").click(function () {
    $("#map-toggle").show();
    $(".section-year-marker").show();
    $("#population-toggle").hide();
  });

  $(".side-navigation-buttons__link--2").click(function () {
    $("#map-toggle").hide();
    $(".section-year-marker").hide();
    $("#population-toggle").show();
  });
  // });


  // Media center gallery center
  var $mediaPicture = $(".media-picture");
  var $mediaPictureLinks = $(".media-picture a");

  var $mediaVideo = $(".media-video");
  var $mediaVideoLinks = $(".media-video a");

  var $mediaPictureButton = $("#mediaPictureButton");
  var $mediaVideoButton = $("#mediaVideoButton");

  $mediaPictureButton.click(function () {
    const mediaPicture = gsap.timeline();

    $("#loadMorePicture").siblings().fadeOut();
    $("#loadMorePicture").fadeIn();

    $mediaPictureButton.siblings().removeClass("btn--media-active");
    $mediaPictureButton.addClass("btn--media-active");
    $mediaPicture.siblings().removeClass("media-active");
    $mediaPicture.addClass("media-active");
    $mediaPicture.siblings().fadeOut(200);
    $mediaPicture.delay(100).fadeIn();

    mediaPicture.from($mediaPictureLinks, {
      opacity: 0.7,
      duration: 0.9,
      scale: 0.8,
    });
  });

  $mediaVideoButton.click(function () {
    const mediaVideo = gsap.timeline();

    $("#loadMoreVideo").siblings().fadeOut();
    $("#loadMoreVideo").fadeIn();

    $mediaVideoButton.siblings().removeClass("btn--media-active");
    $mediaVideoButton.addClass("btn--media-active");
    $mediaVideo.siblings().removeClass("media-active");
    $mediaVideo.addClass("media-active");
    $mediaVideo.siblings().fadeOut(200);
    $mediaVideo.delay(100).fadeIn();
    mediaVideo.from($mediaVideoLinks, {
      opacity: 0.7,
      duration: 0.9,
      scale: 0.8,
    });
  });

  // Checkbox Dropdown
  var $checkBoxDropdown = $(".custom-checkbox #has-dropdown");
  $checkBoxDropdown.click(function () {
    var $eachLabels = $(this).siblings(".custom-checkbox__dropdown").children();

    $(this).siblings(".custom-checkbox__dropdown").toggleClass("active");

    var $dropdownLabelTl = gsap.timeline();
    $dropdownLabelTl.from($eachLabels, {
      duration: 0.2,
      opacity: 0,
      y: 10,
      x: 5,
      stagger: 0.1,
    });
  });

  // Load more button
  // $(function () {
  //   $("div").slice(0, 4).show();
  //   $("#loadMore").on("click", function (e) {
  //     e.preventDefault();
  //     $("div:hidden").slice(0, 4).fadeIn();
  //     if ($("div:hidden").length == 0) {
  //       $("#load").fadeOut("slow");
  //     }
  //     $("html,body").animate(
  //       {
  //         scrollTop: $(this).offset().top,
  //       },
  //       1500
  //     );
  //   });
  // });

  // $("a[href=#top]").click(function () {
  //   $("body,html").animate(
  //     {
  //       scrollTop: 0,
  //     },
  //     600
  //   );
  //   return false;
  // });

  // $(window).scroll(function () {
  //   if ($(this).scrollTop() > 50) {
  //     $(".totop a").fadeIn();
  //   } else {
  //     $(".totop a").fadeOut();
  //   }
  // });

  $(function () {
    $(".media-picture > a").slice(0, 6).show();
    $("#loadMorePicture").on("click", function (e) {
      e.preventDefault();
      $(".media-picture > a:hidden").slice(0, 6).fadeIn();
    });
  });

  $(function () {
    $(".publication-content__publications > a").slice(0, 2).show();
    $("#loadMorePub").on("click", function (e) {
      e.preventDefault();
      $(".publication-content__publications > a:hidden").slice(0, 2).fadeIn();
      // if ($("div:hidden").length == 0) {
      //   $("#load").fadeOut("slow");
      // }
      // $("html,body").animate(
      //   {
      //     scrollTop: $(this).offset().top,
      //   },
      //   1500
      // );
    });
  });

  $(function () {
    $(".media-video > a").slice(0, 3).show();
    $("#loadMoreVideo").on("click", function (e) {
      e.preventDefault();
      $(".media-video > a:hidden").slice(0, 3).fadeIn();
      // if ($("div:hidden").length == 0) {
      //   $("#load").fadeOut("slow");
      // }
      // $("html,body").animate(
      //   {
      //     scrollTop: $(this).offset().top,
      //   },
      //   1500
      // );
    });
  });

    // Slider toggler ================================
    var $leftToggle = $("#leftToggle");
    var $rightToggle = $("#rightToggle");
    var $overallSlider= $(".slider-holder__overall-pop");
    var $electoralViolenceSlider = $(".slider-holder__electoral-vio");
  
    $leftToggle.click(function(){
      $electoralViolenceSlider.slideUp(200);
      $overallSlider.delay(100).slideDown();
    })
  
    $rightToggle.click(function(){
      $overallSlider.slideUp(200);
      $electoralViolenceSlider.delay(100).slideDown();
    });
  
    // (function(){
    var slideIndex = 0;
    
    function showSlides() {
      var i;
      var slides = document.getElementsByClassName("count-numbers");
      for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
      }
      slideIndex++;
      console.log(slides.length);
      if (slideIndex > slides.length) {slideIndex = 1}
      slides[slideIndex-1].style.display = "block";
      setTimeout(showSlides, 4000); // Change image every 2 seconds
    }
    // })();
});


// Image gallery
// const lightBox = document.createElement("div");
// lightBox.className = "light-box";
// document.body.appendChild(lightBox);

const lightBox = document.querySelector(".light-box");

const images = document.querySelectorAll("#lightbox a");
images.forEach((image) => {
  image.addEventListener("click", (e) => {
    e.preventDefault();
    lightBox.classList.add("active");
    const img = document.createElement("img");
    img.src = image.href;
    img.style.maxWidth = "80%";
    img.style.maxHeight = "80vh";

    lightBox.firstChild ? lightBox.removeChild(lightBox.firstChild) : null;
    lightBox.appendChild(img);
  });
});
try {
  lightBox.addEventListener("click", (e) => {
    if (e.target !== e.currentTarget) return;
    lightBox.classList.remove("active");
  });
} catch (error) {}

// Reports Popup
const reportPopup = document.querySelector(".report-popup");
const reports = document.querySelectorAll(".report-cards .report-card");
reports.forEach((report) => {
  report.addEventListener("click", (e) => {
    reportPopup.classList.add("active");
  });
});
try {
  reportPopup.addEventListener("click", (e) => {
    if (e.target !== e.currentTarget) return;
    reportPopup.classList.remove("active");
  });
} catch (error) {}

// Gsap outside Jquery ===================================================================
// const topFIlter = document.querySelector(".nav-filter__filters");
// const dataFIlter = document.querySelector(".data-filter");
// gsap.registerPlugin(ScrollTrigger);

// const filtersTl = gsap.timeline();
// filtersTl.to(topFIlter, {
//   scrollTrigger: {
//     trigger: dataFIlter,
//     start: "top 95%",
//     scrub: true,
//     toggleActions: "restart restart none none",
//   },
//   y: -300,
//   duration: 0.2,
// });

  